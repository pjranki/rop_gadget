import os
import sys

import ropgadget.args
import ropgadget.binary
import ropgadget.core
import ropgadget.gadgets
import ropgadget.options
import ropgadget.rgutils
import ropgadget.updateAlert
import ropgadget.version
import ropgadget.loaders
import ropgadget.ropchain

from capstone import Cs, CS_ARCH_ARM, CS_MODE_THUMB, CS_ARCH_ARM64, CS_MODE_ARM


class ROPGadget(object):

    def __init__(self, **kwargs):
        args = list()
        for key, value in kwargs.items():
            if key == 'binary':
                assert os.path.exists(value)
            args.extend(['--{}'.format(key), value])
        args = ropgadget.args.Args(args).getArgs()
        self.core = ropgadget.core.Core(args)

    @property
    def gadgets(self):
        _gadgets = self.core.gadgets()
        if not _gadgets:
            save_stdout = sys.stdout
            sys.stdout = None
            self.core.analyze()
            sys.stdout = save_stdout
        gadgets =  self.core.gadgets()
        md = Cs(self.cs_arch, self.cs_mode)
        fixed_gadgets = list()
        for gadget in gadgets:
            gadget['cs_arch'] = self.cs_arch
            gadget['cs_mode'] = self.cs_mode
            del gadget['decodes']
            gadget['decodes'] = list()
            addr = gadget['vaddr']
            code = gadget['bytes'][:]
            ins_list = [ins_text.strip() for ins_text in gadget['gadget'].split(';')]
            new_ins_list = list()
            while len(ins_list) > 0:
                ins = list(md.disasm(code, addr))[0]
                gadget['decodes'].append(ins)
                ins_text = '{} {}'.format(ins.mnemonic, ins.op_str).strip()
                new_ins_list.append(ins_text)
                code = code[ins.size:]
                addr += ins.size
                ins_list = ins_list[1:]
            gadget['gadget'] = ' ; '.join(new_ins_list)
            fixed_gadgets.append(gadget)
        return fixed_gadgets

    @property
    def cs_arch(self):
        return self.core._Core__binary.getArch()

    @property
    def cs_mode(self):
        if self.cs_arch == CS_ARCH_ARM64:
            return CS_MODE_ARM
        return self.core._Core__binary.getArchMode()


def main():
    path = sys.argv[1]
    binary = ROPGadget(binary=path)
    for gadget in binary.gadgets:
        print(gadget['gadget'])


if __name__ == '__main__':
    main()
